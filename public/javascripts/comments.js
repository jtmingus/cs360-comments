
$(document).ready(function(){
    $("#serialize").click(function(){
        var myobj = {Name:$("#Name").val(),Comment:$("#Comment").val()};
        jobj = JSON.stringify(myobj);
	console.log("JOB OBJECT: ", jobj)
        $("#json").text(jobj);
	var url = "comment";
	$.ajax({
  	  url:url,
  	  type: "POST",
	  data: jobj,
	  contentType: "application/json; charset=utf-8",
	  success: function(data,textStatus) {
      		$("#done").html(textStatus);
	  }
	})    
    });

  $("#getThem").click(function() {
      $.getJSON('comments', function(data) {
        console.log(data);
        var commCount = {};
	var everything = "<ul>";
        for(var comment in data) {
          com = data[comment];
          everything += "<li>Name: " + com.Name + " -- Comment: " + com.Comment + "</li>";
          if (com.Name in commCount) {
		commCount[com.Name] += 1;
	  } else {
		commCount[com.Name] = 1;
	  }
	}
        everything += "</ul><br><h4>Number of Comments per Person</h4>";
	everything += "<ul>";
	for(var name in commCount) {
	  everything += "<li>" + name + ": " + commCount[name] + "</li>";
	}
	everything += "</ul>";
        $("#comments").html(everything);
      })
    })

});
